var argscheck = require('cordova/argscheck');
var exec = require('cordova/exec');

var PLUGIN_NAME = 'ProvaDeVidaPlugin';

var pv;

function ProvaDeVidaPlugin() {
    /*Base64*/
    this.foto = null;
    this.isAlive = false;
    this.errorMessage = null;
    pv = this;
}

// The function that passes work along to native shells
ProvaDeVidaPlugin.prototype.provaDeVida = function(onAfterSuccess, onAfterError) {
    var successCallback = function (info) {
        pv.foto = info.foto;
        pv.isAlive = info.isAlive;

        console.log("FOTO: " + info.foto);
        console.log("IS_ALIVE: " + info.isAlive);

        onAfterSuccess();
    }
    var errorCallback = function (e) {
      pv.errorMessage = e;

      onAfterError();
    }
    cordova.exec(successCallback, errorCallback, PLUGIN_NAME, 'provaDeVida', []);
}

ProvaDeVidaPlugin.install = function() {
  if (!window.plugins) {
    window.plugins = {};
  }
  window.plugins.ProvaDeVidaPlugin = new ProvaDeVidaPlugin();
  return window.plugins.ProvaDeVidaPlugin;
};

cordova.addConstructor(ProvaDeVidaPlugin.install);
