package br.gov.serpro.dv

/**
 * TODO Detecção de Vida
 * Possíveis Estados da Detecção de Vida.
 */
enum class DVEstado {

    /**
     * Estado Inicial. Nada detectado. Esperando Detecção de Face Única.
     */
    ESPERANDO_FACE,

    /**
     * Face única detectada. Esperando o piscar do olho esquerdo.
     */
    ESPERANDO_PISQUE_OLHO_ESQUERDO,

    /**
     * Pisque do olho esquerdo detectado. Esperando o piscar do olho direito.
     */
    ESPERANDO_PISQUE_DIREITO,

    /**
     * Pisque do olho direito detectado. Esperando sorriso.
     */
    ESPERANDO_SORRISO,

    /**
     * Olhos abertos e sem sorriso
     */
    ESPERANDO_SELFIE,

    /**
     * Todas as Detecções foram concluídas com sucesso.
     * Após este estado, basta capturar o frame ou tirar uma foto do usuário e usá-la.
     */
    DETECCAO_CONCLUIDA
}
