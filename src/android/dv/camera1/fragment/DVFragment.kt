package br.gov.serpro.dv.camera1.fragment

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageFormat
import android.hardware.Camera
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import br.gov.serpro.R
import br.gov.serpro.dv.DVEstado
import br.gov.serpro.dv.DetectorDeVida
import br.gov.serpro.dv.camera1.util.CameraUtils
import br.gov.serpro.dv.camera1.view.CameraPreview
import br.gov.serpro.dv.util.ToastUtils
import br.gov.serpro.dv.util.logarInfo
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import kotlinx.android.synthetic.main.dv_fragment.*

/**
 * TODO Detecção de Vida
 * Um [Fragment] que implementa o processo de Detecção de Vida.
 * É responsável por controlar a [Camera] (API 1), seu Preview e as chamadas ao [DetectorDeVida].
 *
 * Esta implementação é baseada no guia oficial:
 * https://developer.android.com/guide/topics/media/camera
 *
 * Activities que contém este fragmento devem implementar a interface [DVFragment.Delegate]
 * para tratar seus eventos.
 *
 * Use o factory method [DVFragment.newInstance] para criar uma nova instancia dessa classe.
 *
 * TODO Melhorar layout usando Overlays
 * TODO Mover Camera.open(int) para uma Thread própria (p/ não bloquear a Thread de UI)
 */
class DVFragment : Fragment(), DetectorDeVida.Delegate, CameraPreview.Delegate {

    /**
     * Esta interface deve ser implementada pelas activities que contém este fragmento para que
     * possam tratar seus eventos corretamente.
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface Delegate {

        /**
         * Callback chamada ao final do processo de detecção de vida.
         *
         * @param imagemFace ByteArray com a imagem resultante da detecção.
         */
        fun onSucessoDeteccaoDeVida(face: Bitmap)

        /**
         * Callback de erro chamada durante erros que ocorram durante a detecção.
         *
         * @param e Exceção ocorrida.
         */
        fun onErroDuranteDeteccao(e: Exception)
    }

    companion object {
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_LIMITE_OLHO_ESQUERDO = "ARG_LIMITE_OLHO_ESQUERDO"
        private const val ARG_LIMITE_OLHO_DIREITO = "ARG_LIMITE_OLHO_DIREITO"
        private const val ARG_LIMITE_SORRISO = "ARG_LIMITE_SORRISO"

        /**
         * Use este factory method para criar uma nova instância deste fragmento,
         * passando os parâmetros necessários.
         *
         * @param limiteOlhoEsquerdo Limite para o percentual de detecção de pisque do olho esquerdo
         * @param limiteOlhoDireito Limite para o percentual de detecção de pisque do olho direito
         * @param limiteSorriso Limite para o percentual de detecção de sorriso
         * @return Uma nova instancia de [DVFragment].
         */
        @JvmStatic
        fun newInstance(limiteOlhoEsquerdo: Float, limiteOlhoDireito: Float, limiteSorriso: Float) =
                DVFragment().apply {
                    arguments = Bundle().apply {
                        putFloat(ARG_LIMITE_OLHO_ESQUERDO, limiteOlhoEsquerdo)
                        putFloat(ARG_LIMITE_OLHO_DIREITO, limiteOlhoDireito)
                        putFloat(ARG_LIMITE_SORRISO, limiteSorriso)
                    }
                }
    }

    private var delegate: Delegate? = null

    /**
     * [Camera], objeto que mantém a conexão com o hardware.
     * Lembre-se sempre de liberá-la em onPause().
     */
    private var camera: Camera? = null

    /**
     * Id da Camera.
     * @see [Camera.getNumberOfCameras]
     */
    private var cameraId = 1

    /**
     * [SurfaceView] que renderizará o Preview da Câmera.
     */
    private var cameraPreview: CameraPreview? = null

    /**
     * Player de beeps de progresso (tocados durante os eventos de detecção).
     */
    private var beepPlayer: MediaPlayer? = null

    /**
     * Objeto que encapsula as chamadas ao Firebase ML Kit e coordena o processo de detecção,
     * devolvendo eventos via [DetectorDeVida.Delegate].
     */
    private var detectorDeVida: DetectorDeVida? = null

    /**
     * Estado corrente da detecção.
     */
    private var estadoDeteccao = DVEstado.ESPERANDO_FACE

    /**
     * Id da Face que está atualmente realizando o processo de Detecção de Vida.
     */
    private var faceId: Int? = null

    /**
     * Primeira callback do ciclo de vida do [Fragment].
     * Chamada após a anexação deste fragmento pela Host Activity.
     *
     * @param context Host Activity
     */
    override fun onAttach(context: Context) {
        super.onAttach(context)

        // Verifica se a Host Activity realmente implementa a interface [Delegate]
        if (context is Delegate) {
            this.delegate = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    /**
     * Segunda callback do ciclo de vida de criação e inicialização do [Fragment].
     *
     * Como esta inicialização é chamada antes da criação da View (onCreateView), você pode usar
     * esta callback para inicializar valores necessários para a criação da sua View, por exemplo.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val args = arguments
        if (args == null) {
            this.detectorDeVida = DetectorDeVida(this)
        } else {
            val argLimiteOlhoEsquerdo = args.getFloat(ARG_LIMITE_OLHO_ESQUERDO)
            val argLimiteOlhoDireito = args.getFloat(ARG_LIMITE_OLHO_DIREITO)
            val argLimiteSorriso = args.getFloat(ARG_LIMITE_SORRISO)

            val limites = DetectorDeVida.Limites(
                    olhoEsquerdo = argLimiteOlhoEsquerdo,
                    olhoDireito = argLimiteOlhoDireito,
                    sorriso = argLimiteSorriso
            )

            this.detectorDeVida = DetectorDeVida(this, limites)
        }
    }

    /**
     * Terceira callback do ciclo de vida de criação e inicialização do [Fragment].
     * Responsável por inicializar a View deste Fragment.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dv_fragment, container, false)
    }

    /**
     * Quarta callback do ciclo de vida de criação e inicialização do [Fragment].
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // The documentation states that onActivityCreated() is the recommended place to find and
        // store references to your views.
        // https://stackoverflow.com/questions/6495898/findviewbyid-in-fragment

        // Tenta obter a primeira camera frontal do aparelho
        val idCameraFrontal = CameraUtils.selecionarPrimeiraCameraFrontal()
        if (idCameraFrontal == null) {
            val erroMsg = "Câmera frontal não detectada"
            //ToastUtils.logAndToast(context, erroMsg, level = Log.ERROR)
        } else {
            this.cameraId = idCameraFrontal
        }

        // Inicializando o Player do beep de progresso
        this.beepPlayer = MediaPlayer.create(context, R.raw.beep)
    }

    /**
     * Sexta callback do ciclo de vida de criação e inicialização do [Fragment].
     */
    override fun onResume() {
        super.onResume()

        setupCam()
    }

    /**
     * Primeira callback do ciclo de vida de destruição do [Fragment].
     * Libere recursos obtidos em [onResume].
     */
    override fun onPause() {
        // Release the Camera!
        releaseCam()

        // Liberando os recursos dos filhos antes dos pais...
        // https://stackoverflow.com/questions/18821481/what-is-the-correct-order-of-calling-superclass-methods-in-onpause-onstop-and-o
        super.onPause()
    }

    /**
     * Terceira callback do ciclo de vida de destruição do [Fragment].
     * Libere recursos obtidos em [onActivityCreated] e [onCreateView] respectivamente.
     */
    override fun onDestroyView() {
        this.beepPlayer = null

        // You must clean up these stored references by setting them back to null in onDestroyView()
        // or you will leak the Activity.
        // https://stackoverflow.com/questions/6495898/findviewbyid-in-fragment

        super.onDestroyView()
    }

    override fun onDestroy() {
        this.detectorDeVida = null

        super.onDestroy()
    }

    /**
     * Quinta e última callback do ciclo de vida do [Fragment].
     * Libere recursos obtidos em [onAttach].
     */
    override fun onDetach() {
        this.delegate = null

        super.onDetach()
    }

    /**
     * Configura e abre uma conexão com a Camera
     */
    private fun setupCam() {
        try {
            //logarInfo("Abrindo uma nova conexão com a camera...")

            // Abre a conexão com a câmera. O id corresponde a qual câmera deverá ser aberta.
            // Calling Camera.open() throws an RuntimeException if the camera is already in use by
            // another application, so we wrap it in a try block.
            // TODO Mover para uma thread própria
            this.camera = Camera.open(this.cameraId)

            // NonNull Casting aqui é seguro, pois caso não haja câmera, uma exceção será disparada
            // logo acima
            val cam = this.camera as Camera

            // Loga Informações úteis sobre a Camera do Dispositivo
            CameraUtils.logSupportedPreviewFormats(cam)
            CameraUtils.logSupportedPreviewSizes(cam)
            CameraUtils.logPreferredPreviewSizeForVideo(cam)
            CameraUtils.logSupportedFpsRange(cam)

            // Criando o Preview da Camera
            val ctx = context ?: throw RuntimeException(
                    "Não é possível inicializar o preview da câmera"
            )
            this.cameraPreview = CameraPreview(ctx, cam, this.cameraId, this)

            // Set the Preview view as the content of our activity.
            cameraFrameLayout.addView(this.cameraPreview)

        } catch (e: Exception) {
            val msg = "Não foi possível inicializar camera $cameraId"
            ToastUtils.logAndToast(context, msg, level = Log.ERROR, exception = e)
        }
    }

    /**
     * Libera os recursos da câmera.
     */
    private fun releaseCam() {
        cameraFrameLayout.removeAllViews() // resolvendo o problema de congelar o frame quando vai pra fundo o app
        this.cameraPreview?.safelyStopPreview()

        // É necessário definir o callback de preview da câmera para null, pois, após seu release,
        // ainda podem haver chamadas a sua callback, resultando em exceções...
        this.camera?.setPreviewCallback(null)

        // Libera a câmera para uso por outras aplicações
        this.camera?.release()

        this.cameraPreview = null
        this.camera = null
    }

    /**
     * [CameraPreview.Delegate]
     * Callback chamada a cada Frame capturado pela câmera
     * (sob o controle de fps do [CameraPreview]).
     */
    override fun onCameraPreviewFrame(data: ByteArray, camera: Camera) {
        if (detectorDeVida?.isBusy == true) {
            logarInfo("Frame Drop: Detector de Vida está ocupado...")
            return
        }

        val previewSize = camera.parameters.previewSize
        logarInfo("CameraPreview::onPreviewFrame - PreviewSize: (${previewSize.width}, ${previewSize.height})")

        val previewFormat = camera.parameters.previewFormat
        val previewFormatLabel = CameraUtils.getSupportedPreviewFormatLabel(previewFormat)
        logarInfo("CameraPreview::onPreviewFrame - PreviewFormat: $previewFormatLabel")

        // Converte formato da camera para formato do ML Vision
        val firebaseImageFormat = previewFormat.let {
            when (it) {
                ImageFormat.NV21 -> FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21
                ImageFormat.YV12 -> FirebaseVisionImageMetadata.IMAGE_FORMAT_YV12
                else -> {
                    logarInfo("O firebase não suporta o formato de preview utilizado. Falling back to NV21")
                    FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21
                }
            }
        }

        // Define a rotação de ajuste necessária para a imagem de acordo com a orientação
        // da camera
        val firebaseRotation = CameraUtils.getFirebaseRotationCompensationFromCameraId(cameraId)

        // (480x360 image size is typically sufficient for image recognition)
        val metadata = FirebaseVisionImageMetadata.Builder()
                .setWidth(previewSize.width)
                .setHeight(previewSize.height)
                .setFormat(firebaseImageFormat)
                .setRotation(firebaseRotation) // Camera traseira: 90. Camera frontal: 270
                .build()

        val image = FirebaseVisionImage.fromByteArray(data, metadata)

        // Seleciona qual ação de detecção realizar de acordo com o estado do processo de
        // detecção de vida
        when (this.estadoDeteccao) {
            DVEstado.ESPERANDO_FACE ->
                this.detectorDeVida?.detectarFace(image)

            DVEstado.ESPERANDO_PISQUE_OLHO_ESQUERDO ->
                this.detectorDeVida?.detectarPisqueEsquerdo(image, this.faceId as Int)

            DVEstado.ESPERANDO_PISQUE_DIREITO ->
                this.detectorDeVida?.detectarPisqueDireito(image, this.faceId as Int)

            DVEstado.ESPERANDO_SORRISO ->
                this.detectorDeVida?.detectarSorriso(image, this.faceId as Int)

            DVEstado.ESPERANDO_SELFIE ->
                this.detectorDeVida?.detectarSelfie(image, this.faceId as Int)

            DVEstado.DETECCAO_CONCLUIDA -> {
                logarInfo("Detecção concluída com Sucesso!")

                // tenta converter o frame da camera para Bitmap
                val bitmap = CameraUtils.bitmapJpegFromCameraPreviewFrame(camera, cameraId, data)
                if (bitmap == null) {
                    this.delegate?.onErroDuranteDeteccao(RuntimeException("Não foi possível obter bitmap do preview da camera"))
                    return
                }

                // Pronto! A foto encontra-se em image/data, pronta para uso!
                dispatchSucessoDeteccaoDeVida(bitmap)
            }
        }
    }

    /**
     * Reinicia o estado da Detecção de Vida.
     */
    private fun resetDetectionState() {
        this.faceId = null
        this.estadoDeteccao = DVEstado.ESPERANDO_FACE

        instrucaoTextView.text = resources.getString(R.string.app_dv_instrucoes_reset)
        instrucaoImageView.setImageResource(R.drawable.frontal_branco)
    }

    /**
     * [DVDelegate]
     * Callback chamada pelo Detector de Vida em caso de erros do Face Detection do ML Kit
     * durante da detecção.
     * Na ocorrência deste evento, o processo de detecção deve ser reiniciado.
     */
    override fun onErroNaDeteccao(e: Exception) {
        // Reiniciando o estado da detecção
        resetDetectionState()

        // Subindo tratamento de exceção para a activity/fragment host
        this.delegate?.onErroDuranteDeteccao(e)
    }

    /**
     * [DVDelegate]
     * Callback chamada pelo Detector de Vida, quando nenhuma face é detectada pelo Face Detection
     * do ML Kit.
     * Na ocorrência deste evento, o processo de detecção deve ser reiniciado.
     */
    override fun onNenhumaFaceDetectada() {
        logarInfo("Nenhuma Face Detectada")

        // Reiniciando o estado da detecção
        resetDetectionState()
    }

    /**
     * [DVDelegate]
     * Callback chamada pelo Detector de Vida quando multiplas faces são detectadas pelo Face
     * Detection do ML Kit.
     * Na ocorrência deste evento, o processo de detecção deve ser reiniciado.
     */
    override fun onMultiplasFacesDetectadas() {
        logarInfo("Multiplas Faces Detectada")

        // Reiniciando o estado da detecção
        resetDetectionState()

        // Atualizando Views de informações e instruções úteis para a detecção de vida
        instrucaoTextView.text = resources.getString(R.string.app_dv_instrucoes_centralize)
        instrucaoImageView.setImageResource(R.drawable.frontal_branco)
    }

    /**
     * [DVDelegate]
     * Callback chamada pelo [DetectorDeVida] quando uma única face é detectada com sucesso.
     */
    override fun onFaceUnicaDetectada(face: FirebaseVisionFace) {
        logarInfo("Face única detectada. Face Id: ${face.trackingId}")
        logarInfo("Prosseguindo com a prova de vida...")
        beepPlayer?.start()

        // Avançando o estado da detecção
        this.faceId = face.trackingId
        this.estadoDeteccao = DVEstado.ESPERANDO_PISQUE_OLHO_ESQUERDO

        // Atualizando Views de informações e instruções úteis para a detecção de vida
        instrucaoTextView.text = resources.getString(R.string.app_dv_instrucoes_piscar_olho_direito)
        instrucaoImageView.setImageResource(R.drawable.p_direito_branco)
    }

    /**
     * [DVDelegate]
     * Callback chamada pelo [DetectorDeVida] quando o piscar do olho esquerdo é detectado com
     * sucesso.
     */
    override fun onPiscarOlhoEsquerdo(face: FirebaseVisionFace) {
        logarInfo("Pisque do olho esquerdo detectado para face ${face.trackingId}.")

        if (this.estadoDeteccao == DVEstado.ESPERANDO_PISQUE_OLHO_ESQUERDO) {
            logarInfo("Prosseguindo com a prova de vida...")
            beepPlayer?.start()

            // Avançando o estado da detecção
            this.faceId = face.trackingId
            this.estadoDeteccao = DVEstado.ESPERANDO_PISQUE_DIREITO

            // Atualizando Views de informações e instruções úteis para a detecção de vida
            instrucaoTextView.text = resources.getString(R.string.app_dv_instrucoes_piscar_olho_esquerdo)
            instrucaoImageView.setImageResource(R.drawable.p_esquerdo_branco)
        }
    }

    /**
     * [DVDelegate]
     * Callback chamada pelo [DetectorDeVida] quando o piscar do olho direito é detectado com
     * sucesso.
     */
    override fun onPiscarOlhoDireito(face: FirebaseVisionFace) {
        logarInfo("Pisque do olho direito detectado para face ${face.trackingId}.")

        if (this.estadoDeteccao == DVEstado.ESPERANDO_PISQUE_DIREITO) {
            logarInfo("Prosseguindo com a prova de vida...")
            beepPlayer?.start()

            // Avançando o estado da detecção
            this.faceId = face.trackingId
            this.estadoDeteccao = DVEstado.ESPERANDO_SORRISO

            // Atualizando Views de informações e instruções úteis para a detecção de vida
            instrucaoTextView.text = resources.getString(R.string.app_dv_instrucoes_sorria)
            instrucaoImageView.setImageResource(R.drawable.sorriso_branco)
        }
    }

    /**
     * [DVDelegate]
     * Callback chamada pelo [DetectorDeVida] quando o sorriso é detectado com sucesso.
     */
    override fun onSorrisoDetectado(face: FirebaseVisionFace) {
        logarInfo("Sorriso detectado para face ${face.trackingId}.")

        if (this.estadoDeteccao == DVEstado.ESPERANDO_SORRISO) {
            logarInfo("Prosseguindo para selfie.")
            beepPlayer?.start()

            // Avançando o estado da detecção
            this.faceId = face.trackingId
            this.estadoDeteccao = DVEstado.ESPERANDO_SELFIE

            // Atualizando Views de informações e instruções úteis para a detecção de vida
            instrucaoTextView.text = resources.getString(R.string.app_dv_selfie_datavalid)
            instrucaoImageView.setImageResource(R.drawable.frontal_branco)
        }
    }

    /**
     * [DVDelegate]
     * Callback chamada pelo [DetectorDeVida] quando o sorriso é detectado com sucesso.
     */
    override fun onSelfieDetectado(face: FirebaseVisionFace) {
        logarInfo("Selfie detectado para face ${face.trackingId}.")

        if (this.estadoDeteccao == DVEstado.ESPERANDO_SELFIE) {
            logarInfo("Prova de vida concluída com sucesso.")
            beepPlayer?.start()

            // Avançando o estado da detecção
            this.faceId = face.trackingId
            this.estadoDeteccao = DVEstado.DETECCAO_CONCLUIDA

            // Atualizando Views de informações e instruções úteis para a detecção de vida
            instrucaoTextView.text = resources.getString(R.string.app_dv_concluida)
            instrucaoImageView.setImageResource(0)
        }
    }

    /**
     * Detecção de Vida concluído com sucesso.
     * Despachando evento para a Activity Host.
     */
    private fun dispatchSucessoDeteccaoDeVida(face: Bitmap) {
        this.delegate?.onSucessoDeteccaoDeVida(face)
    }
}
