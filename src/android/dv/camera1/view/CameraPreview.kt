package br.gov.serpro.dv.camera1.view

import android.content.Context
import android.hardware.Camera
import android.view.SurfaceHolder
import android.view.SurfaceView
import br.gov.serpro.dv.camera1.util.CameraUtils
import br.gov.serpro.dv.util.logarErro
import br.gov.serpro.dv.util.logarInfo

/**
 * TODO Detecção de Vida
 * Um [SurfaceView] para renderizar o preview da câmera.
 *
 * @see <a href="https://developer.android.com/guide/topics/media/camera">Camera API Guide</a>
 */
class CameraPreview(context: Context, private val camera: Camera, private val cameraId: Int, private val delegate: Delegate)
    : SurfaceView(context), SurfaceHolder.Callback, Camera.PreviewCallback {

    /**
     * Delegate de iteração desta view para a activity/fragment host.
     */
    interface Delegate {

        /**
         * Chamado quando um frame for capturado.
         */
        fun onCameraPreviewFrame(data: ByteArray, camera: Camera)
    }

    companion object {
        private const val TAG = "CameraPreview"
    }

    init {
        holder.addCallback(this)
    }

    /**
     * [SurfaceHolder.Callback]
     */
    override fun surfaceCreated(holder: SurfaceHolder) {
        logarInfo("CameraPreview::surfaceCreated")

        // empty. surfaceChanged will take care of stuff
    }

    /**
     * [SurfaceHolder.Callback]
     */
    override fun surfaceChanged(holder: SurfaceHolder, format: Int, w: Int, h: Int) {
        logarInfo("CameraPreview::surfaceChanged - format: $format, w: $w, h: $h")

        // preview surface does not exist
        if (holder.surface == null) {
            logarInfo("CameraPreview::surfaceChanged Surface do Preview da Camera ainda não existe")
            return
        }

        // stop preview before making changes
        // (Make sure to stop the preview before resizing or reformatting it.)
        safelyStopPreview()

        // set preview size and make any resize, rotate or
        // reformatting changes here
        // (If your preview can change or rotate, take care of those events here)
        try {
            camera.setPreviewDisplay(holder)

            // TODO Substituir por setPreviewCallbackWithBuffer para melhorar a performance
            camera.setPreviewCallback(this)
            //camera.setPreviewCallbackWithBuffer(this)

            val displayOrientation = CameraUtils.calcularCameraDisplayOrientation(context, cameraId, camera)
            logarInfo("Definindo orientacao do display da câmera para: $displayOrientation")
            // Starting from API level 14,
            // this method can be called when preview is active
            camera.setDisplayOrientation(displayOrientation)

            // Correção da Rotação da Câmera
            // https://stackoverflow.com/questions/20064793/android-how-to-fix-camera-orientation
            // https://developer.android.com/reference/android/hardware/Camera.Parameters

            // Definindo o parâmetro PreviewSize da camera
            // (Tamanho da Imagem de Preview que será gerada pela câmera)
            val supportedPreviewSizes = camera.parameters.supportedPreviewSizes
            val optimalPreviewSize = CameraUtils.getOptimalPreviewSize(supportedPreviewSizes, w, h)
            val finalPreviewSize = optimalPreviewSize
                    ?: supportedPreviewSizes[supportedPreviewSizes.size - 1]
            logarInfo("Preview Size selecionado para Preview: (${finalPreviewSize.width}, ${finalPreviewSize.height})")
            camera.parameters.setPreviewSize(finalPreviewSize.width, finalPreviewSize.height)

            // Definindo o parâmetro PreviewFpsRange da camera. (Velocidade de captura da camera)
            // Utilizando o menor máximo valor suportado
            val supportedPreviewFpsRange = camera.parameters.supportedPreviewFpsRange
            val optimalPreviewFpsRange = CameraUtils.getSlowestPreviewFpsRange(supportedPreviewFpsRange)
            if (optimalPreviewFpsRange != null) {
                logarInfo("Fps Range selecionado: (${optimalPreviewFpsRange[0]}, ${optimalPreviewFpsRange[1]})")
                camera.parameters.setPreviewFpsRange(optimalPreviewFpsRange[0], optimalPreviewFpsRange[1])
            }

            // start preview with new settings
            camera.startPreview()

        } catch (e: Exception) {
            logarErro(e, "Erro ao configurar preview da camera: ${e.message}")
        }
    }

    /**
     * [SurfaceHolder.Callback]
     */
    override fun surfaceDestroyed(holder: SurfaceHolder) {
        logarInfo("CameraPreview::surfaceDestroyed")
        // noop. A liberação da camera será feita corretamente na host activity/fragment
    }

    /**
     * [Camera.PreviewCallback]
     * Método chamado a cada Frame do Preview.
     */
    override fun onPreviewFrame(data: ByteArray?, camera: Camera?) {
        if (data != null && camera != null) {

            // Despachando frame ao delegate
            this.delegate.onCameraPreviewFrame(data, camera)

        } else {
            logarInfo("Dropando Frame {data: $data, camera: $camera}...")
        }
    }

    /**
     * Interrompe a renderização do preview da câmera.
     * (necessário antes de se realizar alterações no preview)
     */
    fun safelyStopPreview() {
        try {
            camera.stopPreview()

        } catch (e: Exception) {
            // ignore: tried to stop a non-existent preview
            logarInfo("Tentando parar um preview inexistente. É seguro ignorá-lo.")
        }
    }
}
