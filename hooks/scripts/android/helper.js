var fs = require("fs");
var path = require("path");

function rootBuildGradleExists() {
  var target = path.join("platforms", "android", "build.gradle");
  return fs.existsSync(target);
}

function appBuildGradleExists() {
  var target = path.join("platforms", "android", "app", "build.gradle");
  return fs.existsSync(target);
}

/*
 * Helper function to read the build.gradle that sits at the root of the project
 */
function readRootBuildGradle() {
  var target = path.join("platforms", "android", "build.gradle");
  return fs.readFileSync(target, "utf-8");
}

/*
 * Helper function to read the build.gradle that sits at the app module of the project
 */
function readAppBuildGradle() {
  var target = path.join("platforms", "android", "app", "build.gradle");
  return fs.readFileSync(target, "utf-8");
}

/*
 * Added a dependency on 'com.google.gms' based on the position of the know 'com.android.tools.build' dependency in the build.gradle
 */
function addDependencies(buildGradle) {

  console.log('addDependencies');
  // find the known line to match
  var match = buildGradle.match(/^(\s*)classpath 'com.android.tools.build(.*)/m);

  var whitespace = match[1];

  // modify the line to add the necessary dependencies
  var googlePlayDependency = whitespace + 'classpath \'com.google.gms:google-services:4.2.0\' // google-services dependency from cordova-plugin-prova-de-vida';
  var kotlinGradlePlugin = whitespace + 'classpath \'org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.50\' // kotlin gradle plugin from cordova-plugin-prova-de-vida'
  var kotlinAndroidExtentions = whitespace + 'classpath \'org.jetbrains.kotlin:kotlin-android-extensions:1.3.50\' // kotlin gradle plugin from cordova-plugin-prova-de-vida'
  var modifiedLine = match[0] + '\n' + googlePlayDependency + '\n' + kotlinGradlePlugin + '\n' + kotlinAndroidExtentions;
  
  // modify the actual line
  return buildGradle.replace(/^(\s*)classpath 'com.android.tools.build(.*)/m, modifiedLine);
}

/*
 * Add 'google()' and Crashlytics to the repository repo list
 */
function addRepos(buildGradle) {
  console.log('addRepos');
  // find the known line to match
  var match = buildGradle.match(/^(\s*)jcenter\(\)/m);
  var whitespace = match[1];

  /*// modify the line to add the necessary repo
  // Crashlytics goes under buildscripts which is the first grouping in the file
  var fabricMavenRepo = whitespace + 'maven { url \'https://maven.fabric.io/public\' } // Fabrics Maven repository from cordova-plugin-prova-de-vida'
  var modifiedLine = match[0] + '\n' + fabricMavenRepo;

  // modify the actual line
  buildGradle = buildGradle.replace(/^(\s*)jcenter\(\)/m, modifiedLine);*/

  var modifiedLine = match[0];

  // update the all projects grouping
  var allProjectsIndex = buildGradle.indexOf('allprojects');
  if (allProjectsIndex > 0) {
    // split the string on allprojects because jcenter is in both groups and we need to modify the 2nd instance
    var firstHalfOfFile = buildGradle.substring(0, allProjectsIndex);
    var secondHalfOfFile = buildGradle.substring(allProjectsIndex);

    // Add google() to the allprojects section of the string
    match = secondHalfOfFile.match(/^(\s*)jcenter\(\)/m);
    var googlesMavenRepo = whitespace + 'google() // Google\'s Maven repository from cordova-plugin-prova-de-vida';
    modifiedLine = match[0] + '\n' + googlesMavenRepo;
    // modify the part of the string that is after 'allprojects'
    secondHalfOfFile = secondHalfOfFile.replace(/^(\s*)jcenter\(\)/m, modifiedLine);

    // recombine the modified line
    buildGradle = firstHalfOfFile + secondHalfOfFile;
  } else {
    // this should not happen, but if it does, we should try to add the dependency to the buildscript
    match = buildGradle.match(/^(\s*)jcenter\(\)/m);
    var googlesMavenRepo = whitespace + 'google() // Google\'s Maven repository from cordova-plugin-prova-de-vida';
    modifiedLine = match[0] + '\n' + googlesMavenRepo;
    // modify the part of the string that is after 'allprojects'
    buildGradle = buildGradle.replace(/^(\s*)jcenter\(\)/m, modifiedLine);
  }

  return buildGradle;
}

/*
 * Applies the google services plugin 'com.google.gms.google-services'
 */
function applyPlugin(buildGradle) {
  return buildGradle + '\n apply plugin: \'com.google.gms.google-services\' // Google services plugin from cordova-plugin-prova-de-vida';
}

/*
 * Helper function to write to the build.gradle that sits at the root of the project
 */
function writeRootBuildGradle(contents) {
  var target = path.join("platforms", "android", "build.gradle");
  fs.writeFileSync(target, contents);
}

/*
 * Helper function to write to the build.gradle that sits at the app module of the project
 */
function writeAppBuildGradle(contents) {
  var target = path.join("platforms", "android", "app", "build.gradle");
  fs.writeFileSync(target, contents);
}

module.exports = {

  modifyRootBuildGradle: function() {
    // be defensive and don't crash if the file doesn't exist
    if (!rootBuildGradleExists) {
      return;
    }

    console.log("modifyRootBuildGradle");

    var buildGradle = readRootBuildGradle();

    // Add Google Play Services Dependency
    buildGradle = addDependencies(buildGradle);
  
    // Add Google's Maven Repo
    buildGradle = addRepos(buildGradle);

    writeRootBuildGradle(buildGradle);
  },

  restoreRootBuildGradle: function() {
    // be defensive and don't crash if the file doesn't exist
    if (!rootBuildGradleExists) {
      return;
    }

    console.log("restoreRootBuildGradle");

    var buildGradle = readRootBuildGradle();

    // remove any lines we added
    buildGradle = buildGradle.replace(/(?:^|\r?\n)(.*)cordova-plugin-prova-de-vida*?(?=$|\r?\n)/g, '');
  
    writeRootBuildGradle(buildGradle);
  },

  restoreAppBuildGradle: function() {
      // be defensive and don't crash if the file doesn't exist
      if (!appBuildGradleExists) {
        return;
      }

      console.log("restoreAppBuildGradle");

      var buildGradle = readAppBuildGradle();

      // remove any lines we added
      buildGradle = buildGradle.replace(/(?:^|\r?\n)(.*)cordova-plugin-prova-de-vida*?(?=$|\r?\n)/g, '');

      writeAppBuildGradle(buildGradle);
    },

    modifyAppBuildGradle: function() {
        // be defensive and don't crash if the file doesn't exist
        if (!appBuildGradleExists) {
          return;
        }

        console.log("modifyAppBuildGradle");

        var buildGradle = readAppBuildGradle();

        buildGradle = applyPlugin(buildGradle);

        writeAppBuildGradle(buildGradle);
      }
};
